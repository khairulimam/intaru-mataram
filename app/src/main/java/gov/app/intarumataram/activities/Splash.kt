package gov.app.intarumataram.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import gov.app.intarumataram.R
import org.jetbrains.anko.startActivity

class Splash : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_splash)
    Thread(Runnable {
      Thread.sleep(1000)
      startActivity<MapsActivity>()
      finish()
    }).start()
  }
}

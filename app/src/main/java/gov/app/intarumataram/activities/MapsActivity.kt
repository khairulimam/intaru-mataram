package gov.app.intarumataram.activities

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.text.TextUtils
import android.util.Log
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import gov.app.intarumataram.R
import gov.app.intarumataram.data.DataCollector
import gov.app.intarumataram.data.filter.LocationCriteria
import gov.app.intarumataram.data.pojo.Shapes
import gov.app.intarumataram.data.sources.HttpDataSource
import gov.app.intarumataram.data.sources.PolaDataSource
import gov.app.intarumataram.data.sources.ZntDataSource
import gov.app.intarumataram.requests.PlaceDetailAPI
import gov.app.intarumataram.utils.SimpleLocation
import gov.app.intarumataram.utils.format
import gov.app.intarumataram.utils.gone
import gov.app.intarumataram.utils.visible
import kotlinx.android.synthetic.main.activity_maps.*
import org.jetbrains.anko.*
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import kotlin.math.roundToInt


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, SimpleLocation.Listener {

  private lateinit var mMap: GoogleMap
  private lateinit var locationHelper: SimpleLocation
  private lateinit var dataDisplayerCollector: DataCollector
  private var dataSource: HttpDataSource? = null
  private var radius: Double = 0.0

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_maps)
    setupLocation()
    setupMap()
    setupPlaceApi()
    setupDefaultFilter()
    ivFilter.setOnClickListener {
      showFilterDialog()
    }

    ivLocation.setOnClickListener {
      try {
        dataSource?.let {
          tvAccuracy.text = getAccuracyText(locationHelper.lastLocation.accuracy, it.sourceLabel(), radius)
          dataDisplayerCollector.resetIsFirst()
          dataDisplayerCollector.displayDataInRadius(
            locationHelper.lastLocation,
            it,
            radius,
            showResult(),
            showProgress()
          )
        }
      } catch (e:Exception){
     }
    }
  }

  private fun setupDefaultFilter() {
    dataSource = PolaDataSource()
    radius = LocationCriteria.RADIUS
  }

  private fun showFilterDialog() {
    alert {
      title = "Filter sumber data"
      isCancelable = false
      customView {
        verticalLayout {
          padding = dip(20)
          textView("Sumber Data")
          radioGroup {
            radioButton {
              text = context.getString(R.string.znt)
              setOnClickListener {
                dataSource = ZntDataSource()
              }
            }
            radioButton {
              text = context.getString(R.string.pola)
              setOnClickListener {
                dataSource = PolaDataSource()
              }
            }
          }
          textView("Munculkan data dalam radius (meter)")
          val radius = editText {
            hint = "Default ${LocationCriteria.RADIUS} meter"
            inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
          }
          positiveButton("Simpan filter") {
            radius.text.toString().apply {
              if (!(isEmpty() || this.toDouble() < LocationCriteria.RADIUS)) this@MapsActivity.radius = this.toDouble()
            }
          }
        }
      }
      negativeButton("Kembali") {}
    }.show()
  }

  private fun setupPlaceApi() {
    Places.initialize(this, getString(R.string.API_KEY))
    (autocomplete_fragment as AutocompleteSupportFragment).apply {
      setHint("Cari nama tempat")
      setPlaceFields(listOf(Place.Field.ID, Place.Field.NAME))
      setOnPlaceSelectedListener(object : PlaceSelectionListener {
        @SuppressLint("SetTextI18n")
        override fun onPlaceSelected(place: Place) {
          place.id?.let {
            PlaceDetailAPI(this@MapsActivity).getDetail(it) { latLng ->
              val reqLocation = Location("PLACE API").apply {
                latitude = latLng.latitude
                longitude = latLng.longitude
                accuracy = 0f
              }
              dataSource?.let { it1 ->
                this@MapsActivity.tvAccuracy.text = getAccuracyText(reqLocation.accuracy, it1.sourceLabel(), radius)
                dataDisplayerCollector.resetIsFirst()
                dataDisplayerCollector.displayDataInRadius(reqLocation, it1, radius, showResult(), showProgress())
              }
            }
          }
        }

        override fun onError(status: Status) {
          Log.i(localClassName, status.statusMessage)
        }
      })
    }
  }

  private fun showProgress(): (Int) -> Unit = {
    polygonDisplayProgressContainer.visible()
    progressBar.progress = it
    tvResultHuman.text =
      "${(it.toDouble() / progressBar.max.toDouble()).times(100).format(2)}%"
    if (it == progressBar.max)
      polygonDisplayProgressContainer.gone()
  }

  private fun showResult(): (List<Shapes>) -> Unit = {
    progressBar.max = it.size
    tvAccuracy.apply {
      text = "$text\nJumlah bidang: ${it.size}.\nTotal koordinat: ${it.map { it.polygon.size * 2 }.sum()}"
    }
  }

  private fun setupLocation() {
    locationHelper = SimpleLocation(
      this,
      true,
      false,
      updateIntervalInMilliseconds
    )
    if (!locationHelper.hasLocationEnabled()) {
      alert("Anda harus mengaktifkan GPS jika ingin melihat bidang disekitar anda") {
        positiveButton("Buka Pengaturan") {
          SimpleLocation.openSettings(this@MapsActivity)
          it.dismiss()
        }
        negativeButton("Kembali") {}
      }.show()
    }
    locationHelper.setListener(this)
    locationHelper.beginUpdates()
  }

  private fun setupMap() {
    val mapFragment = supportFragmentManager
      .findFragmentById(R.id.map) as SupportMapFragment
    mapFragment.getMapAsync(this)
  }

  override fun onMapReady(googleMap: GoogleMap) {
    mMap = googleMap
    mMap.setOnMapClickListener {
      tvAttribute.apply {
        text = ""
        gone()
      }
      dataDisplayerCollector.polygonDrawer.previousClickedPolygon?.strokeWidth = 1f
    }
    requireAccessFineLocation()
    dataDisplayerCollector = DataCollector(this, mMap, showAttribute())
  }

  private fun showAttribute(): (Map<String, Any>) -> Unit = {
    val capKeys = it.keys
      .map { key -> "${TextUtils.join(" ", key.toUpperCase().split("_"))}: ${it[key]}" }
      .toList()
    val data = TextUtils.join("\n", capKeys)
    tvAttribute.text = data
    tvAttribute.visible()
  }

  override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
  }

  override fun onPositionChanged() {
    dataSource?.let {
      tvAccuracy.text = getAccuracyText(locationHelper.lastLocation.accuracy, it.sourceLabel(), radius)
      dataDisplayerCollector.displayDataInRadius(locationHelper.lastLocation, it, radius, showResult(), showProgress())
    }
  }

  private fun getAccuracyText(acc: Float, dataSource: String, radius: Double): String =
    "Menampilkan data dalam radius $radius meter.\nAkurasi GPS: ${acc.roundToInt()} meter.\nSumber data: $dataSource."

  @SuppressLint("MissingPermission")
  @AfterPermissionGranted(RC_LOCATION)
  private fun requireAccessFineLocation() {
    val perms = arrayOf(ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION)
    if (EasyPermissions.hasPermissions(this, *perms)) {
      mMap.isMyLocationEnabled = true
      mMap.uiSettings.isMyLocationButtonEnabled = false
    } else {
      // Do not have permissions, request them now
      EasyPermissions.requestPermissions(
        this@MapsActivity,
        getString(R.string.location_rationale),
        RC_LOCATION,
        *perms
      )
    }
  }

  companion object {
    private const val RC_LOCATION = 0
    // locationHelper will be updated every # in minutes
    var updateIntervalInMilliseconds = (0.5 * 60 * 1000).toLong()
  }

}

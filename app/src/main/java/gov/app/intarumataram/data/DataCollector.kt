package gov.app.intarumataram.data

import android.app.ProgressDialog
import android.content.Context
import android.location.Location
import android.util.Log
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.loopj.android.http.JsonHttpResponseHandler
import cz.msebera.android.httpclient.Header
import gov.app.intarumataram.data.filter.LocationCriteria
import gov.app.intarumataram.data.pojo.Shapes
import gov.app.intarumataram.data.sources.HttpDataSource
import gov.app.intarumataram.requests.Http
import gov.app.intarumataram.utils.getTm3Zone
import gov.app.intarumataram.utils.latLng
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import org.json.JSONArray

class DataCollector(
  private val context: Context,
  private val map: GoogleMap,
  showAttribute: (Map<String, Any>) -> Unit
) {

  val polygonDrawer = PolygonDrawer(map, showAttribute)
  private val criteria = LocationCriteria()
  private var isFirstLocation = true

  fun resetIsFirst() {
    isFirstLocation = true
  }

  fun displayDataInRadius(
    location: Location,
    dataSource: HttpDataSource,
    radius: Double,
    dataResult: (List<Shapes>) -> Unit,
    dataProcessed: (Int) -> Unit
  ) {
    criteria.onNewLocation(location)
    map.animateCamera(CameraUpdateFactory.newLatLngZoom(location.latLng(), 19f))
    if (criteria.isTolerantableAccuracy() && criteria.isTolerantableDifference() || isFirstLocation) {
      isFirstLocation = false
      getDataForValidLocation(location, dataSource, radius, dataResult, dataProcessed)
    }
  }

  private fun showLoading(): ProgressDialog {
    val loadingDialog =
      context.indeterminateProgressDialog("Sedang mengambil data bidang pada lokasi", "Mohon menunggu") {
        setCancelable(false)
      }
    loadingDialog.show()
    return loadingDialog
  }

  private fun getDataForValidLocation(
    location: Location,
    dataSource: HttpDataSource,
    radius: Double,
    dataResult: (List<Shapes>) -> Unit,
    dataProcessed: (Int) -> Unit
  ) {
    criteria.userLocationOnDataRequested(location)
    val zone = location.latLng().getTm3Zone()
    val loading = showLoading()
    Http.client.get(
      dataSource.endPoint(),
      dataSource.params(criteria, radius),
      JsonDataHandler(loading) { data ->
        val listType = object : TypeToken<List<Shapes>>() {}.type
        val items = Gson().fromJson<List<Shapes>>(data, listType)
        dataResult(items)
        if (items.isEmpty())
          context.alert("Tidak ada data bidang yang ditemukan untuk lokasi saat ini") {
            positiveButton("Kembali") {}
          }.show()
        else zone?.let {
          polygonDrawer.draw(items, it, dataProcessed)
        }
      })
  }

  inner class JsonDataHandler(private val loading: ProgressDialog, val onSuccess: (String) -> Unit) :
    JsonHttpResponseHandler() {

    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, response: JSONArray?) {
      loading.dismiss()
      onSuccess(response.toString())
    }

    override fun onFailure(
      statusCode: Int,
      headers: Array<out Header>?,
      responseString: String?,
      throwable: Throwable?
    ) {
      Log.i("Maps", "${throwable?.localizedMessage}\n$responseString")
      Toast.makeText(context, "Ada kesalahan: ${throwable?.localizedMessage}\n$responseString", Toast.LENGTH_LONG)
        .show()
      loading.dismiss()
    }
  }

}
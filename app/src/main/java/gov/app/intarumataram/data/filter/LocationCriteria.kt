package gov.app.intarumataram.data.filter

import android.location.Location
import com.google.android.gms.maps.model.LatLng
import gov.app.intarumataram.utils.latLng
import gov.app.intarumataram.utils.getNewCoordinateWith

class LocationCriteria {

  private lateinit var userLocationOnDataRequested: Location
  private lateinit var lastLocation: Location

  fun onNewLocation(location: Location) {
    this.lastLocation = location
  }

  fun userLocationOnDataRequested(location: Location) {
    this.userLocationOnDataRequested = location
  }

  private fun getDifference() = this.userLocationOnDataRequested.distanceTo(this.lastLocation)

  fun isTolerantableAccuracy() = this.lastLocation.accuracy <= ACCURACY_THRESHOLD

  fun isTolerantableDifference() = try {
    getDifference() >= RADIUS
  } catch (e: UninitializedPropertyAccessException) {
    false
  }

  fun getSouthWest(radius: Double): LatLng =
    lastLocation.latLng().getNewCoordinateWith(315.0, radius)

  fun getNorthEast(radius: Double): LatLng =
    lastLocation.latLng().getNewCoordinateWith(135.0, radius)

  companion object {
    //in meters
    const val RADIUS = 100.0
    const val ACCURACY_THRESHOLD = 20f
  }

}


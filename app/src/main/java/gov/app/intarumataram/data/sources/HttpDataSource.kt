package gov.app.intarumataram.data.sources

import com.loopj.android.http.RequestParams
import gov.app.intarumataram.data.filter.LocationCriteria

abstract class HttpDataSource {
  abstract fun endPoint(): String
  abstract fun params(criteria: LocationCriteria, radius: Double): RequestParams
  abstract fun sourceLabel(): String
}
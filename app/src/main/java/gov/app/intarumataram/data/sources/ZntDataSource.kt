package gov.app.intarumataram.data.sources

import android.util.Log
import com.loopj.android.http.RequestParams
import gov.app.intarumataram.data.filter.LocationCriteria

open class ZntDataSource : HttpDataSource() {

  override fun sourceLabel(): String = "ZNT"
  override fun endPoint(): String = "https://syc.smartptsl.com/service-znt-mataram.php"

  override fun params(criteria: LocationCriteria, radius: Double): RequestParams {
    val p1 = criteria.getSouthWest(radius)
    val p2 = criteria.getNorthEast(radius)
    return RequestParams().apply {
      put("x1", p1.latitude)
      put("x2", p2.latitude)
      put("y1", p1.longitude)
      put("y2", p2.longitude)
      Log.i("Maps", toString())
    }
  }
}
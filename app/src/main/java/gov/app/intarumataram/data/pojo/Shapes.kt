package gov.app.intarumataram.data.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Shapes(
  @SerializedName("attribut")
  @Expose
  var attribut: Map<String, Any>,
  @SerializedName("polygon")
  @Expose
  var polygon: List<List<Double>>,
  @SerializedName("fill")
  @Expose
  var fill: String,
  @SerializedName("stroke")
  @Expose
  var stroke: String
)
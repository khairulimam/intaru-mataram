package gov.app.intarumataram.data.sources

class PolaDataSource : ZntDataSource() {

  override fun endPoint(): String = "https://syc.smartptsl.com/service-pola-ruang.php"

  override fun sourceLabel(): String = "Pola"

}